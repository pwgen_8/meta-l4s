SUMMARY = "csp networking library"
HOMEPAGE = "https://github.com/libcsp/libcsp"
SECTION = "libs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2915dc85ab8fd26629e560d023ef175c"

SRC_URI = "git://github.com/libcsp/libcsp;nobranch=1;protocol=https;rev=b2996d29b492e5d1dcaafc4cc01ab2367d540a11;"


S = "${WORKDIR}/git"

inherit cmake  pkgconfig

#DEPENDS += ""


